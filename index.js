var http = require('http');
var fs = require('fs');
var path = require('path');
var pact = require('@pact-foundation/pact-node');
var startedServers = [];

// Config
var httpPort = process.env.httpPort || 8080;
var upperMockPort = process.env.upperMockPort || 20100;
var lowerMockPort = process.env.lowerMockPort || 20000;

http.createServer(function (req, res) {
    console.log(req.method, req.url, req.headers['content-type'])
    // Endpoint to create Server
    if (req.method === 'POST' && req.url === '/servers' && req.headers['content-type'] === 'application/json') {
        createServer(req, function (port) {
            if (port) {
                res.writeHead(201, {'Location': port})
            } else {
                res.writeHead(409);
            }
            res.end();
        });
    } else if (req.method === 'GET' && req.url === '/servers') {
        listServers(function (servers) {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify(servers));
            res.end();
        });
    } else if (req.method === 'DELETE' && req.url === '/servers') {
        stopServers(req, function (success) {
            if (success) {
                res.writeHead(204);
            } else {
                res.writeHead(404);
            }
            res.end();
        });
    } else {
        res.statusCode = 400;
        res.end();
    }
}).listen(httpPort);
console.info('Server started');

function createServer(req, callback) {
    //Read Pact File From Request
    readBodyData(req, function (body) {
        var pactFile = JSON.parse(body);
        var pactFilename = pactFile['consumer']['name'] + '-' + pactFile['provider']['name'] + '.json';

        fs.writeFile(pactFilename, body, function (err) {
            if (err) {
                return console.log(err);
            }
            //After File has been written start Server
            var port = getFreePort(startedServers);
            if (port) {
                var server = pact.createStub({
                    cors: true,
                    port: port,
                    host: '0.0.0.0',
                    pactUrls: [__dirname + path.sep + pactFilename]
                });
                pact.logLevel('debug');
                server.start().then(function (value) {
                    startedServers.push({
                        name: pactFilename,
                        port: value.options.port,
                        serverObj: server
                    });
                    callback(value.options.port);
                })
            } else {
                callback(false);
            }
        });
    });
}

function listServers(callback) {
    var servers = startedServers
        .map(function (server) {
            return {name: server.name, port: server.port}
        });
    callback(servers);
}

function stopServers(req, callback) {
    var port = req.headers['x-pact-port'];
    console.log(port);
    var server = startedServers.filter(function (s) {
        console.log(s.port, Number(port));
        return s.port === Number(port);
    })[0];
    if (server) {
        console.log(startedServers.indexOf(server));
        startedServers.splice(startedServers.indexOf(server), 1);
        server['serverObj'].stop().then(function () {
            callback(true)
        });
    } else {
        callback(false);
    }
}

function readBodyData(req, callback) {
    var body = [];
    req.on('data', function (chunk) {
        body.push(chunk);
    }).on('end', function () {
        body = Buffer.concat(body).toString();
        callback(body);
    });
}

function getFreePort(servers) {
    var diff = upperMockPort - lowerMockPort;
    var counter = 0;
    var desiredPort;
    do {
        desiredPort = generateRandomInteger(lowerMockPort, upperMockPort);
        counter++;
        if (counter > diff) {
            return false;
        }
    } while (!portAvailable(servers, desiredPort));
    return desiredPort;
}

function generateRandomInteger(min, max) {
    return Math.floor(min + Math.random() * (max + 1 - min))
}

function portAvailable(servers, port) {
    var available = true;
    servers.forEach((function (server) {
        if (server.port === port) {
            available = false;
        }
    }));
    return available;
}