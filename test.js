servers = [
    {port: 10},
    {port: 12}
]

function portAvailable(servers, port){
    var available = true;
    servers.forEach((function(server){
        console.log(server.port, port);
        if(server.port === port){
            available = false;
        }
    }));
    return available;
}

console.log(portAvailable(servers, 10));
console.log(portAvailable(servers, 12));
console.log(portAvailable(servers, 11));